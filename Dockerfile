FROM openjdk
MAINTAINER Dawid Cisowski <dawid.cisowski@outlook.com>
ADD target/zoo-config-server.jar zoo-config-server.jar
ENTRYPOINT ["java", "-jar", "zoo-config-server.jar"]
EXPOSE 8031