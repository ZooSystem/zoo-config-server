package com.zoosystem.zooconfigserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ZooConfigServerApplication {

  public static void main(String[] args) {
    SpringApplication.run(ZooConfigServerApplication.class, args);
  }
}
